import asyncio
import os
import random
from itertools import cycle

import discord
import youtube_dl
from discord.ext import commands, tasks
from discord.ext.commands import Bot
from discord.utils import get
from discord import FFmpegPCMAudio
from os import system
import shutil

messages = joined = 0

client = commands.Bot (command_prefix='rtd.',
                       command_description='Hi I am Retard Bot. I give you very un-true information')

status = cycle (['do rtd.help to get started! ps, just do the say command whenever', 'im  a little stupid',
                 'look at my description'])
songs = asyncio.Queue ( )
play_next_song = asyncio.Event ( )

players = {}

math = ['ballsack', 'cracka penis', ]
science = ['wawa bitch im a baby', 'i sit on children', 'i have lou gerigs disease', 'you built like stephen hawking', ]
ss = ['boutta toss this tea bruh', 'THEY ARE IN THE TREES', 'run forrest run!', 'banned from chruch like stalin', ]
ela = [
    'We the People of the United States, in Order to form a more perfect Union, establish Justice, insure domestic Tranquility, provide for the common defense, promote the general Welfare, and secure the Blessings of Liberty to ourselves and our Posterity, do ordain and establish this Constitution for the United States of America.',
    'Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.',
    'Iceland is a free and sovereign state, resting on the cornerstones of freedom, equality, democracy and human rights. The government shall work for the welfare of the inhabitants of the country, strengthen their culture and respect the diversity of human life, the land and the biosphere', ]


@client.event
async def on_ready() :
    await client.change_presence (activity=discord.Game ('look at my description!'), status=discord.Status.online)
    change_status.start ( )
    print ('bot is ready')


# moderation commands

@tasks.loop (seconds=10)
async def change_status() :
    await client.change_presence (activity=discord.Game (next (status)))


@client.command ( )
async def kick(ctx, member: discord.Member, *, reason=None) :
    """kicks a member of the discord"""
    await member.kick (reason=reason)


@client.command ( )
async def ban(ctx, member: discord.Member, *, reason=None) :
    """bans a memeber of the discord"""
    await member.ban (reason=reason)
    await ctx.send (f'Banned {member.name, member.discriminator}')


@client.command ( )
async def unban(ctx, *, member) :
    """unbans a banned user of the discord"""
    banned_users = await ctx.guild.bans ( )
    member_name, member_discriminator = member.split ('#')

    for ban_entry in banned_users :
        user = ban_entry.user

        if (user.name, user.discriminator) == (member_name, member_discriminator) :
            await ctx.guild.unban (user)
            await ctx.send (f'Unbanned {user.name}#{user.discriminator}')


@client.command ( )
async def clear(ctx, amount=10) :
    """clears spam/messages to make space"""
    await ctx.channel.purge (limit=amount)


# music/voice commands
@client.event
async def on_ready():
    print("Logged in as: " + bot.user.name + "\n")


@client.command(pass_context=True, aliases=['j', 'joi'])
async def join(ctx):
    global voice
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()

    await voice.disconnect()

    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
        print(f"The bot has connected to {channel}\n")

    await ctx.send(f"Joined {channel}")


@client.command(pass_context=True, aliases=['l', 'lea'])
async def leave(ctx):
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_connected():
        await voice.disconnect()
        print(f"The bot has left {channel}")
        await ctx.send(f"Left {channel}")
    else:
        print("Bot was told to leave voice channel, but was not in one")
        await ctx.send("Don't think I am in a voice channel")


@client.command(pass_context=True, aliases=['p', 'pla'])
async def play(ctx, url: str):

    def check_queue():
        Queue_infile = os.path.isdir("./Queue")
        if Queue_infile is True:
            DIR = os.path.abspath(os.path.realpath("Queue"))
            length = len(os.listdir(DIR))
            still_q = length - 1
            try:
                first_file = os.listdir(DIR)[0]
            except:
                print("No more queued song(s)\n")
                queues.clear()
                return
            main_location = os.path.dirname(os.path.realpath(__file__))
            song_path = os.path.abspath(os.path.realpath("Queue") + "\\" + first_file)
            if length != 0:
                print("Song done, playing next queued\n")
                print(f"Songs still in queue: {still_q}")
                song_there = os.path.isfile("song.mp3")
                if song_there:
                    os.remove("song.mp3")
                shutil.move(song_path, main_location)
                for file in os.listdir("./"):
                    if file.endswith(".mp3"):
                        os.rename(file, 'song.mp3')

                voice.play(discord.FFmpegPCMAudio("song.mp3"), after=lambda e: check_queue())
                voice.source = discord.PCMVolumeTransformer(voice.source)
                voice.source.volume = 0.07

            else:
                queues.clear()
                return

        else:
            queues.clear()
            print("No songs were queued before the ending of the last song\n")



    song_there = os.path.isfile("song.mp3")
    try:
        if song_there:
            os.remove("song.mp3")
            queues.clear()
            print("Removed old song file")
    except PermissionError:
        print("Trying to delete song file, but it's being played")
        await ctx.send("ERROR: Music playing")
        return


    Queue_infile = os.path.isdir("./Queue")
    try:
        Queue_folder = "./Queue"
        if Queue_infile is True:
            print("Removed old Queue Folder")
            shutil.rmtree(Queue_folder)
    except:
        print("No old Queue folder")

    await ctx.send("Getting everything ready now")

    voice = get(bot.voice_clients, guild=ctx.guild)

    ydl_opts = {
        'format': 'bestaudio/best',
        'quiet': True,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        print("Downloading audio now\n")
        ydl.download([url])

    for file in os.listdir("./"):
        if file.endswith(".mp3"):
            name = file
            print(f"Renamed File: {file}\n")
            os.rename(file, "song.mp3")

    voice.play(discord.FFmpegPCMAudio("song.mp3"), after=lambda e: check_queue())
    voice.source = discord.PCMVolumeTransformer(voice.source)
    voice.source.volume = 0.07

    nname = name.rsplit("-", 2)
    await ctx.send(f"Playing: {nname[0]}")
    print("playing\n")


@client.command(pass_context=True, aliases=['pa', 'pau'])
async def pause(ctx):

    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_playing():
        print("Music paused")
        voice.pause()
        await ctx.send("Music paused")
    else:
        print("Music not playing failed pause")
        await ctx.send("Music not playing failed pause")


@client.command(pass_context=True, aliases=['r', 'res'])
async def resume(ctx):

    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_paused():
        print("Resumed music")
        voice.resume()
        await ctx.send("Resumed music")
    else:
        print("Music is not paused")
        await ctx.send("Music is not paused")


@client.command(pass_context=True, aliases=['s', 'sto'])
async def stop(ctx):
    voice = get(bot.voice_clients, guild=ctx.guild)

    queues.clear()

    if voice and voice.is_playing():
        print("Music stopped")
        voice.stop()
        await ctx.send("Music stopped")
    else:
        print("No music playing failed to stop")
        await ctx.send("No music playing failed to stop")


queues = {}

@client.command(pass_context=True, aliases=['q', 'que'])
async def queue(ctx, url: str):
    Queue_infile = os.path.isdir("./Queue")
    if Queue_infile is False:
        os.mkdir("Queue")
    DIR = os.path.abspath(os.path.realpath("Queue"))
    q_num = len(os.listdir(DIR))
    q_num += 1
    add_queue = True
    while add_queue:
        if q_num in queues:
            q_num += 1
        else:
            add_queue = False
            queues[q_num] = q_num

    queue_path = os.path.abspath(os.path.realpath("Queue") + f"\song{q_num}.%(ext)s")

    ydl_opts = {
        'format': 'bestaudio/best',
        'quiet': True,
        'outtmpl': queue_path,
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        print("Downloading audio now\n")
        ydl.download([url])
    await ctx.send("Adding song " + str(q_num) + " to the queue")

    print("Song added to queue\n")


# meme/fun commands to do

@client.command()
async def saymath(ctx) :
    """says a fact not involving math"""
    await ctx.send (random.choice (math))


@client.command()
async def sayscience(ctx) :
    """says a fact not involving science"""
    await ctx.send (random.choice (science))


@client.command()
async def sayela(ctx) :
    """says a fact not involving ela"""
    await ctx.send (random.choice (ela))


@client.command()
async def sayss(ctx) :
    """says a fact not involving social studies"""
    await ctx.send (random.choice (ss))


token = 'NTk5NzM2OTYwNzk3NTA3NjE0.XSpioA.x2Z0NRbCGcd01vxxzD8nLvEML18'

client.run (token)
